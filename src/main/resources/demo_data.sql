Create database db_grad_cs_1917;
USE db_grad_cs_1917;

--
-- Table structure for table `anonymous_users`
--

DROP TABLE IF EXISTS `anonymous_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anonymous_users` (
  `anonymous_user_id` char(40) NOT NULL,
  `anonymous_user_pwd` char(20) DEFAULT NULL,
  PRIMARY KEY (`anonymous_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anonymous_users`
--

LOCK TABLES `anonymous_users` WRITE;
/*!40000 ALTER TABLE `anonymous_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `anonymous_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counterparty`
--

DROP TABLE IF EXISTS `counterparty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counterparty` (
  `counterparty_id` int(11) NOT NULL,
  `counterparty_name` char(30) NOT NULL,
  `counterparty_status` char(1) DEFAULT NULL,
  `counterparty_date_registered` datetime DEFAULT NULL,
  PRIMARY KEY (`counterparty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



LOCK TABLES `counterparty` WRITE;
/*!40000 ALTER TABLE `counterparty` DISABLE KEYS */;
INSERT INTO `counterparty` VALUES ('001','Lina','A','2014-01-01 00:00:00'),('002','Allison','A','2014-01-01 00:00:00'), ('004','Selvyn','A','2014-01-01 00:00:00'),('003','Estelle','A','2014-01-01 00:00:00'),('005','Lewis','A','2014-01-01 00:00:00');
/*!40000 ALTER TABLE `counterparty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrument`
--

DROP TABLE IF EXISTS `instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instrument` (
  `instrument_id` int(11) NOT NULL,
  `instrument_name` varchar(35) NOT NULL,
  PRIMARY KEY (`instrument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



LOCK TABLES `instrument` WRITE;
/*!40000 ALTER TABLE `instrument` DISABLE KEYS */;
INSERT INTO `instrument` VALUES ('1','Astronomica'),('2','Deuteronic'),('3','Floral'),('4','Galactia'),('5','Celestial');
UNLOCK TABLES;

--
-- Table structure for table `deal`
--

DROP TABLE IF EXISTS `deal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deal` (
  `deal_id` int(11) NOT NULL,
  `deal_counterparty_id` int(11) DEFAULT NULL,
  `deal_instrument_id` int(11) DEFAULT NULL,
  `deal_type` char(1) DEFAULT NULL,
  `deal_amount` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`deal_id`),
  KEY `deal_counterparty_id` (`deal_counterparty_id`),
  KEY `deal_instrument_id` (`deal_instrument_id`),
  CONSTRAINT `deal_ibfk_1` FOREIGN KEY (`deal_counterparty_id`) REFERENCES `counterparty` (`counterparty_id`),
  CONSTRAINT `deal_ibfk_2` FOREIGN KEY (`deal_instrument_id`) REFERENCES `instrument` (`instrument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `deal` WRITE;
/*!40000 ALTER TABLE `deal` DISABLE KEYS */;
INSERT INTO `deal` VALUES (20001,'1','001','S',3405.29),(20002,'2','002','B',8531.05),(20003,'3','003','B',402.61),(20013,'4','005','B',8531.05),(20014,'5','002','B',402.61),(20015,'2','003','S',9960.38),(20005,'3','004','S',9838.68),(20006,'1','005','S',1425.97),(20007,'3','001','B',7908.21),(20008,'4','003','S',9720.69),(20009,'5','002','B',1437.60),(20010,'2','004','B',3438.50),(20011,'1','003','B',3590.63),(20012,'4','001','S',3563.22);
/*!40000 ALTER TABLE `deal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` char(40) NOT NULL,
  `user_pwd` char(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('alison','gradprog2016@07'),('debs','gradprog2016@02'),('estelle','gradprog2016@05'),('john','gradprog2016@03'),('pauline','gradprog2016@04'),('samuel','gradprog2016@06'),('selvyn','gradprog2016');
INSERT INTO users (user_id,user_pwd) VALUES ('isaac','password') ;

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_trail`
--

DROP TABLE IF EXISTS `login_trail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_trail` (
  `login_id` int(11) NOT NULL,
  `logged_in_user_id` char(40) NOT NULL,
  `logged_in_auser_id` char(40) NOT NULL,
  `login_date_and_time` datetime NOT NULL,
  PRIMARY KEY (`login_id`),
  KEY `logged_in_user_id` (`logged_in_user_id`),
  KEY `logged_in_auser_id` (`logged_in_auser_id`),
  CONSTRAINT `login_trail_ibfk_1` FOREIGN KEY (`logged_in_user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `login_trail_ibfk_2` FOREIGN KEY (`logged_in_auser_id`) REFERENCES `anonymous_users` (`anonymous_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
