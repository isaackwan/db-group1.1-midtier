package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

public class EffectiveProfit {

	private String counterparty_name;
	private Double EffectiveProfit;

	public EffectiveProfit() {}

	public EffectiveProfit(String counterparty_name, Double EffectiveProfit) {
		this.counterparty_name = counterparty_name;
		this.EffectiveProfit = EffectiveProfit;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Deal))
			return false;
		if (obj == this)
			return true;

		final EffectiveProfit effectiveProfit = (EffectiveProfit) obj;
		return this.counterparty_name.equals(effectiveProfit.counterparty_name) &&
				this.EffectiveProfit == effectiveProfit.EffectiveProfit;
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(counterparty_name, EffectiveProfit); // impl from https://stackoverflow.com/a/18066516
	}

	public String getCounterparty_name() {
		return counterparty_name;
	}
	public void setCounterparty_name(String counterparty_name) {
		this.counterparty_name = counterparty_name;
	}

	public Double getEffectiveProfit() { return EffectiveProfit; }
	public void setEffectiveProfit(Double EffectiveProfit) {
		this.EffectiveProfit = EffectiveProfit;
	}

}
