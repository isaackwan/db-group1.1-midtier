package com.db.graduatetraining.analyzer.midtier;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class MaskedPiiSerializer extends StdSerializer<String> {
	public MaskedPiiSerializer() {
		this(null);
	}

	public MaskedPiiSerializer(Class<String> t) {
		super(t);
	}

	@Override
	public void serialize(String s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		jsonGenerator.writeString("xxxxx");
	}
}