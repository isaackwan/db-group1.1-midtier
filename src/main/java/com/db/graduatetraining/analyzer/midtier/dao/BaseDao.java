package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * A Data Access Object (DAO) allows you to retrieve, update, etc information from database
 * @see <a href="https://stackoverflow.com/questions/19154202/data-access-object-dao-in-java">this StackOverflow post</a>
 */
public abstract class BaseDao<T extends DomainObject> {
	final protected String tableName;
	private DomainSerializer<T> domainSerializer;

	/**
	 * @param tableName the name of the table in singular form
	 * @param domainSerializer (de-)serializer, you can implement this as an inner class
	 */
	protected BaseDao(String tableName, DomainSerializer<T> domainSerializer) {
		this.domainSerializer = domainSerializer;
		this.tableName = tableName;
	}

	public Collection<T> getAll() throws SQLException {
		ArrayList<T> list = new ArrayList<T>();
		try (Connection conn = ConnectionManager.getInstance().getConnection();) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM %s", tableName));

			while (rs.next()) {
				list.add(domainSerializer.deserialize(rs));
			}

		}
		return list;
	}
}
