package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A serializer turns a Data Transfer Object (DTO) into a stream of strings (= SQL statements)
 * and a deserializer converts results from a DBMS into DTO
 */
public interface DomainSerializer<T extends DomainObject> {
	public T deserialize(ResultSet rs) throws SQLException;
}
