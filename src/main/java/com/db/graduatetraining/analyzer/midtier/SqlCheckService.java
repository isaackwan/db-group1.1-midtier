package com.db.graduatetraining.analyzer.midtier;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlCheckService {
	private DataSource ds;

	public String getDatabaseVersion() throws SQLException {
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT VERSION();");
			rs.next();
			final String sqlVersion = rs.getString(1);
			return sqlVersion;
		} catch (Exception e) {
			return "Cannot getConnection()";
		}
	}
}
