package com.db.graduatetraining.analyzer.midtier.domain;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Anemic Domain Model for User
 */
public class Deal implements DomainObject {
	private int id;
	private int counterpartyId;
	private int instrumentId;
	private String type;
	private BigDecimal amount;
	private int quantity;
	private String time;

	public Deal() {}

	public Deal(int id, int counterpartyId, int instrumentId, String type, BigDecimal amount, int quantity, String time) {
		this.id = id;
		this.counterpartyId = counterpartyId;
		this.instrumentId = instrumentId;
		this.type = type;
		this.amount = amount;
		this.quantity = quantity;
		this.time = time;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Deal))
			return false;
		if (obj == this)
			return true;

		final Deal deal = (Deal) obj;

		return this.id == deal.id &&
				this.counterpartyId == deal.counterpartyId &&
				this.instrumentId == deal.instrumentId &&
				this.type.equals(deal.type) &&
				this.amount.equals(deal.amount);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(id, counterpartyId, instrumentId, type, amount); // impl from https://stackoverflow.com/a/18066516
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCounterpartyId() {
		return counterpartyId;
	}

	public void setCounterpartyId(int counterpartyId) {
		this.counterpartyId = counterpartyId;
	}

	public int getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(int instrumentId) {
		this.instrumentId = instrumentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
}
