package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

public class AveragePrice implements DomainObject  {
	private String name;
	private Double averageBuyPrice;
	private Double averageSellPrice;

	public AveragePrice() {}

	public AveragePrice(String name, Double averageBuyPrice, Double averageSellPrice) {
		this.name = name;
		this.averageBuyPrice = averageBuyPrice;
		this.averageSellPrice = averageSellPrice;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Deal))
			return false;
		if (obj == this)
			return true;

		final AveragePrice averagePrice = (AveragePrice) obj;
		return this.name.equals(averagePrice.name) &&
				this.averageBuyPrice==averagePrice.averageBuyPrice &&
				this.averageSellPrice==averagePrice.averageSellPrice;
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(name, averageBuyPrice, averageSellPrice); // impl from https://stackoverflow.com/a/18066516
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Double getAverageBuyPrice() { return averageBuyPrice; }
	public void setAverageBuyPrice(Double averageBuyPrice) {
		this.averageBuyPrice = averageBuyPrice;
	}

	public Double getAverageSellPrice() { return averageSellPrice; }
	public void setAverageSellPrice(Double averageSellPrice) {
		this.averageSellPrice = averageSellPrice;
	}
}
