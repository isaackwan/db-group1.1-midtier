package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

public class dealsDone implements DomainObject  {
	private String counterparty_name;
	private String instrument_name;
	private int dealsDone;

	public dealsDone() {}

	public dealsDone(String counterparty_name, String instrument_name, int dealsDone) {
		this.counterparty_name = counterparty_name;
		this.instrument_name = instrument_name;
		this.dealsDone = dealsDone;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Deal))
			return false;
		if (obj == this)
			return true;

		final dealsDone DealsDone = (dealsDone) obj;
		return this.counterparty_name.equals(DealsDone.counterparty_name) &&
				this.instrument_name.equals(DealsDone.instrument_name) &&
				this.dealsDone==DealsDone.dealsDone;
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(counterparty_name, instrument_name, dealsDone); // impl from https://stackoverflow.com/a/18066516
	}

	public String getCounterpartyName() {
		return counterparty_name;
	}
	public void setCounterpartyName(String counterparty_name) {
		this.counterparty_name = counterparty_name;
	}

	public String getInstrumentName() { return instrument_name; }
	public void setInstrumentName(String instrument_name) {
		this.instrument_name = instrument_name;
	}

	public int getDealsDone() { return dealsDone; }
	public void setDealsDone(int dealsDone) {
		this.dealsDone = dealsDone;
	}
}
