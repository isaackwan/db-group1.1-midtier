package com.db.graduatetraining.analyzer.midtier.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

public class DealWithRelatedEntities extends Deal {
	private String instrumentName;
	private String counterpartyName;
	private String counterpartyStatus;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private Timestamp counterpartyDateRegistered;

	public DealWithRelatedEntities(int id, int counterpartyId, int instrumentId, String type, BigDecimal amount, int quantity, String time, String instrumentName, String counterpartyName, String counterpartyStatus, Timestamp counterpartyDateRegistered) {
		super(id, counterpartyId, instrumentId, type, amount, quantity, time);
		this.instrumentName = instrumentName;
		this.counterpartyName = counterpartyName;
		this.counterpartyStatus = counterpartyStatus;
		this.counterpartyDateRegistered = counterpartyDateRegistered;
	}

	public DealWithRelatedEntities() {
		super();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DealWithRelatedEntities)) return false;
		DealWithRelatedEntities that = (DealWithRelatedEntities) o;
		return Objects.equals(instrumentName, that.instrumentName) &&
				Objects.equals(counterpartyName, that.counterpartyName) &&
				Objects.equals(counterpartyStatus, that.counterpartyStatus) &&
				Objects.equals(counterpartyDateRegistered, that.counterpartyDateRegistered) &&
				super.equals(that);
	}

	@Override
	public int hashCode() {
		return Objects.hash(instrumentName, counterpartyName, counterpartyStatus, counterpartyDateRegistered, super.hashCode());
	}

	public String getInstrumentName() {
		return instrumentName;
	}

	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}

	public String getCounterpartyName() {
		return counterpartyName;
	}

	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}

	public String getCounterpartyStatus() {
		return counterpartyStatus;
	}

	public void setCounterpartyStatus(String counterpartyStatus) {
		this.counterpartyStatus = counterpartyStatus;
	}

	public Timestamp getCounterpartyDateRegistered() {
		return counterpartyDateRegistered;
	}

	public void setCounterpartyDateRegistered(Timestamp counterpartyDateRegistered) {
		this.counterpartyDateRegistered = counterpartyDateRegistered;
	}
}
