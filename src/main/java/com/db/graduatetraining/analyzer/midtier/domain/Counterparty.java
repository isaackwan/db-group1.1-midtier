package com.db.graduatetraining.analyzer.midtier.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * Anemic Domain Model for User
 */
public class Counterparty implements DomainObject {
	private int id;
	private String name;
	private String status;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private Timestamp dateRegistered;

	public Counterparty() {}

	public Counterparty(int id, String name, String status, Timestamp dateRegistered) {
		this.id = id;
		this.name = name;
		this.status = status;
		this.dateRegistered = dateRegistered;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Counterparty))
			return false;
		if (obj == this)
			return true;

		final Counterparty counterparty = (Counterparty) obj;

		return this.id == counterparty.id &&
				this.name.equals(counterparty.name) &&
				this.status.equals(counterparty.status) &&
				this.dateRegistered.equals(counterparty.dateRegistered);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(id, name, status, dateRegistered); // impl from https://stackoverflow.com/a/18066516
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getDateRegistered() {
		return dateRegistered;
	}

	public void setDateRegistered(Timestamp dateRegistered) {
		this.dateRegistered = dateRegistered;
	}
}
