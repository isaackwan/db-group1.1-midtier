package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.Deal;
import com.db.graduatetraining.analyzer.midtier.domain.DealWithRelatedEntities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class DealDao extends BaseDao<Deal> {
	public DealDao() {
		super("deal", new DealSerializer());
	}

	static protected class DealSerializer implements DomainSerializer<Deal> {
		@Override
		public Deal deserialize(ResultSet rs) throws SQLException {
			return new Deal(
					rs.getInt("deal_id"),
					rs.getInt("deal_counterparty_id"),
					rs.getInt("deal_instrument_id"),
					rs.getString("deal_type"),
					rs.getBigDecimal("deal_amount"),
					rs.getInt("deal_quantity"),
					rs.getString("deal_time")
			);
		}
	}

	public Collection<DealWithRelatedEntities> getAllWithRelatedEntities() throws SQLException {
		ArrayList<DealWithRelatedEntities> list = new ArrayList<>();
		try (Connection conn = ConnectionManager.getInstance().getConnection();) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT deal_id, deal_counterparty_id, deal_instrument_id, deal_type, deal_amount, deal_quantity, deal_time, instrument_name, counterparty_name, counterparty_status, counterparty_date_registered\n" +
					"FROM (SELECT deal_id, deal_counterparty_id, deal_instrument_id, deal_type, deal_amount, deal_quantity, deal_time, instrument_name\n" +
					"FROM deal JOIN instrument ON deal.deal_instrument_id = instrument.instrument_id) TableA JOIN counterparty ON TableA.deal_counterparty_id = counterparty.counterparty_id");

			while (rs.next()) {
				DealWithRelatedEntities current = new DealWithRelatedEntities(
						rs.getInt("deal_id"),
						rs.getInt("deal_counterparty_id"),
						rs.getInt("deal_instrument_id"),
						rs.getString("deal_type"),
						rs.getBigDecimal("deal_amount"),
						rs.getInt("deal_quantity"),
						rs.getString("deal_time"),
						rs.getString("instrument_name"),
						rs.getString("counterparty_name"),
						rs.getString("counterparty_status"),
						rs.getTimestamp("counterparty_date_registered")
				);
				list.add(current);
			}
		}
		return list;
	}
}
