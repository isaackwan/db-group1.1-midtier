package com.db.graduatetraining.analyzer.midtier.domain;

import java.sql.Timestamp;
import java.util.Objects;

public class CounterpartyWithCount extends Counterparty {
	private int count;

	public CounterpartyWithCount() {
		super();
	}

	public CounterpartyWithCount(int id, String name, String status, Timestamp dateRegistered, int count) {
		super(id, name, status, dateRegistered);
		this.count = count;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof CounterpartyWithCount))
			return false;
		if (obj == this)
			return true;

		final CounterpartyWithCount counterparty = (CounterpartyWithCount) obj;

		return this.count == counterparty.count && super.equals(counterparty);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(getId(), getName(), getStatus(), getDateRegistered(), count); // impl from https://stackoverflow.com/a/18066516
	}


	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
