package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.domain.DealHashMap;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public class DealHashMapService {
	public Collection<DealHashMap> getAll() throws SQLException {
		ArrayList<DealHashMap> list = new ArrayList<DealHashMap>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("Select c.counterparty_name,i.instrument_name,  count(d.deal_instrument_id) as DealsDone \n" +
					"from deal d, counterparty c, instrument i\n" +
					"where d.deal_counterparty_id = c.counterparty_id AND i.instrument_id = d.deal_instrument_id\n" +
					"group by d.deal_counterparty_id, d.deal_instrument_id;"));

			HashMap<String, Integer> instrument_deal = new HashMap<String, Integer>();
			while (rs.next()) {
				instrument_deal = new HashMap<String, Integer>();
				String name = rs.getString("counterparty_name");
				rs.previous();
				while (rs.next()) {
					if (name.equals(rs.getString("counterparty_name"))) {
						instrument_deal.put(rs.getString("instrument_name"), rs.getObject("dealsDone") != null ? rs.getInt("dealsDone") : 0);

					} else {
						rs.previous();
						DealHashMap current = new DealHashMap(
								rs.getString("counterparty_name"),
								instrument_deal);
						list.add(current);
						break;
					}
				}
			}
			rs.previous();
			DealHashMap current = new DealHashMap(
					rs.getString("counterparty_name"),
					instrument_deal);
			list.add(current);
		}
		return list;
	}
}
