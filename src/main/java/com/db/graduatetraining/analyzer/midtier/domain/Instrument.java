package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

/**
 * Anemic Domain Model for User
 */
public class Instrument implements DomainObject {
	private int id;
	private String name;

	public Instrument() {}

	public Instrument(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Instrument))
			return false;
		if (obj == this)
			return true;

		final Instrument instrument = (Instrument) obj;

		return this.id == instrument.id &&
				this.name.equals(instrument.name);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(id, name); // impl from https://stackoverflow.com/a/18066516
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
