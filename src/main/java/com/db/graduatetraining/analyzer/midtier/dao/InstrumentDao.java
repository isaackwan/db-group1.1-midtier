package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.Instrument;
import com.db.graduatetraining.analyzer.midtier.domain.InstrumentWithCount;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class InstrumentDao extends BaseDao<Instrument> {
	public InstrumentDao() {
		super("instrument", new InstrumentSerializer());
	}

	static protected class InstrumentSerializer implements DomainSerializer<Instrument> {
		@Override
		public Instrument deserialize(ResultSet rs) throws SQLException {
			return new Instrument(rs.getInt("instrument_id"), rs.getString("instrument_name"));
		}
	}

	public Collection<InstrumentWithCount> getAllWithCount() throws SQLException {
		ArrayList<InstrumentWithCount> list = new ArrayList<InstrumentWithCount>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT instrument_id, instrument_name, 0 as `count` FROM instrument WHERE instrument_id NOT IN (SELECT deal_instrument_id FROM deal)\n" +
					"UNION ALL\n" +
					"(SELECT instrument_id, instrument_name, count(deal_id) AS `count` FROM instrument\n" +
					"JOIN deal\n" +
					"ON instrument.instrument_id = deal.deal_instrument_id\n" +
					"GROUP BY instrument_id)");

			while (rs.next()) {
				InstrumentWithCount current = new InstrumentWithCount(
						rs.getInt("instrument_id"),
						rs.getString("instrument_name"),
						rs.getInt("count")
				);
				list.add(current);
			}
		}
		return list;
	}
}
