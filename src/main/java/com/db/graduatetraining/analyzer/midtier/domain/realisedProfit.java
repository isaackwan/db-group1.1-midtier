package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

public class realisedProfit implements DomainObject{
	private String counterparty_name;
	private Double Profit;

	public realisedProfit() {}

	public realisedProfit(String counterparty_name, Double Profit) {
		this.counterparty_name = counterparty_name;

		this.Profit = Profit;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Deal))
			return false;
		if (obj == this)
			return true;

		final realisedProfit RealisedProfit = (realisedProfit) obj;
		return this.counterparty_name.equals(RealisedProfit.counterparty_name) &&
				this.Profit == RealisedProfit.Profit;
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(counterparty_name, Profit); // impl from https://stackoverflow.com/a/18066516
	}

	public String getCounterparty_name() {
		return counterparty_name;
	}
	public void setCounterparty_name(String counterparty_name) {
		this.counterparty_name = counterparty_name;
	}

	public Double getProfit() { return Profit; }
	public void setProfit(Double Profit) {
		this.Profit = Profit;
	}

}
