package com.db.graduatetraining.analyzer.midtier.domain;

import com.db.graduatetraining.analyzer.midtier.MaskedPiiSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Objects;

/**
 * Anemic Domain Model for User
 */
public class User implements DomainObject {
	private String id;
	private String pwd;

	public User() {}

	public User(String id, String pwd) {
		this.id = id;
		this.pwd = pwd;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof User))
			return false;
		if (obj == this)
			return true;

		final User user = (User) obj;

		return this.id.equals(user.id) &&
				this.pwd.equals(user.pwd);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(id, pwd); // impl from https://stackoverflow.com/a/18066516
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonSerialize(using = MaskedPiiSerializer.class)
	public String getPwd() {
		return pwd;
	}

	@JsonIgnore
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
