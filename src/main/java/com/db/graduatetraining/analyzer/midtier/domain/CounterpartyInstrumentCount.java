package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

public class CounterpartyInstrumentCount {
	private int counterpartyId;
	private int instrumentId;
	private int count;

	public CounterpartyInstrumentCount(int counterpartyId, int instrumentId, int count) {
		this.counterpartyId = counterpartyId;
		this.instrumentId = instrumentId;
		this.count = count;
	}

	public CounterpartyInstrumentCount() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CounterpartyInstrumentCount)) return false;
		CounterpartyInstrumentCount that = (CounterpartyInstrumentCount) o;
		return counterpartyId == that.counterpartyId &&
				instrumentId == that.instrumentId &&
				count == that.count;
	}

	@Override
	public int hashCode() {
		return Objects.hash(counterpartyId, instrumentId, count);
	}

	public int getCounterpartyId() {
		return counterpartyId;
	}

	public void setCounterpartyId(int counterpartyId) {
		this.counterpartyId = counterpartyId;
	}

	public int getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(int instrumentId) {
		this.instrumentId = instrumentId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
