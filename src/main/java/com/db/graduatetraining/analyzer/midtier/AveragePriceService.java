package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.AveragePrice;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public abstract class AveragePriceService<T extends DomainObject> {


	public static Collection<AveragePrice> getAll() throws SQLException {
		ArrayList<AveragePrice> list = new ArrayList<AveragePrice>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("(SELECT A.instrument_name, A.Average_Buy_Price, B.Average_Sell_Price\n" +
					"FROM (SELECT AVG(d1.deal_amount) as Average_Buy_Price, i.instrument_name \n" +
					"FROM instrument i LEFT JOIN deal d1  ON d1.deal_instrument_id = i.instrument_id \n" +
					"WHERE d1.deal_type = 'B' \n" +
					"GROUP BY i.instrument_name) A\n" +
					"LEFT OUTER JOIN \n" +
					"(SELECT AVG(d2.deal_amount) as Average_Sell_Price, i.instrument_name \n" +
					"FROM instrument i LEFT JOIN deal d2  ON d2.deal_instrument_id = i.instrument_id \n" +
					"WHERE d2.deal_type = 'S' \n" +
					"GROUP BY i.instrument_name) B on A.instrument_name = B.instrument_name) \n" +
					"UNION\n" +
					"(SELECT D.instrument_name, C.Average_Buy_Price, D.Average_Sell_Price\n" +
					"FROM \n" +
					"(SELECT AVG(d1.deal_amount) as Average_Buy_Price, i.instrument_name \n" +
					"FROM instrument i LEFT JOIN deal d1  ON d1.deal_instrument_id = i.instrument_id \n" +
					"WHERE d1.deal_type = 'B' \n" +
					"GROUP BY i.instrument_name) C\n" +
					"right OUTER JOIN \n" +
					"(SELECT AVG(d2.deal_amount) as Average_Sell_Price, i.instrument_name \n" +
					"FROM instrument i LEFT JOIN deal d2  ON d2.deal_instrument_id = i.instrument_id \n" +
					"WHERE d2.deal_type = 'S' \n" +
					"GROUP BY i.instrument_name) D on C.instrument_name = D.instrument_name);\n" +
					"\n"));

			while (rs.next()) {
				AveragePrice current= new AveragePrice(
						rs.getString("instrument_name"),
						rs.getObject("Average_Buy_Price") != null ? rs.getDouble("Average_Buy_Price") : null,
						rs.getObject("Average_Sell_Price") != null ? rs.getDouble("Average_Sell_Price") : null
				);
				list.add(current);
			}
		}

		return list;
	}
}
