package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.domain.CounterpartyInstrumentCount;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class CounterpartyInstrumentCountService {
	public Collection<CounterpartyInstrumentCount> getAll() throws SQLException {
		ArrayList<CounterpartyInstrumentCount> list = new ArrayList<>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT deal_counterparty_id AS counterparty_id, deal_instrument_id AS instrument_id, COUNT(*) as `count` FROM deal GROUP BY deal_instrument_id, deal_counterparty_id");

			while (rs.next()) {
				CounterpartyInstrumentCount current = new CounterpartyInstrumentCount(
						rs.getInt("counterparty_id"),
						rs.getInt("instrument_id"),
						rs.getInt("count")
				);
				list.add(current);
			}
		}

		return list;
	}
}
