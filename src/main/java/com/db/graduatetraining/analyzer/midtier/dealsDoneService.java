package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.domain.dealsDone;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;


public abstract class dealsDoneService<T extends DomainObject> {
	public static Collection<dealsDone> getAll() throws SQLException {
		ArrayList<dealsDone> list = new ArrayList<dealsDone>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("Select   c.counterparty_name,i.instrument_name,  count(d.deal_instrument_id) as DealsDone \n" +
					"from deal d, counterparty c, instrument i\n" +
					"where d.deal_counterparty_id = c.counterparty_id AND i.instrument_id = d.deal_instrument_id\n" +
					"group by d.deal_counterparty_id, d.deal_instrument_id;"));

			while (rs.next()) {

				dealsDone current= new dealsDone(
						rs.getString("counterparty_name"),
						rs.getString("instrument_name"),
						rs.getObject("dealsDone") != null ? rs.getInt("dealsDone") : null
				);
				list.add(current);
			}
		}
		return list;
	}
}
