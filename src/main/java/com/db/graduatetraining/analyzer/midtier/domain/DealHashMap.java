package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.HashMap;
import java.util.Objects;

public class DealHashMap {
	private String counterparty_name;
	private HashMap<String , Integer > instrument_deal = new HashMap<String ,Integer>();

	public DealHashMap() {}

	public DealHashMap(String counterparty_name, HashMap<String,Integer> instrument_deal) {
		this.counterparty_name = counterparty_name;
		this.instrument_deal = instrument_deal;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof Deal))
			return false;
		if (obj == this)
			return true;

		final DealHashMap DealsDone = (DealHashMap) obj;
		return this.counterparty_name.equals(DealsDone.counterparty_name) &&
				this.instrument_deal.equals(DealsDone.instrument_deal);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(counterparty_name, instrument_deal); // impl from https://stackoverflow.com/a/18066516
	}

	public String getCounterpartyName() {
		return counterparty_name;
	}
	public void setCounterpartyName(String counterparty_name) {
		this.counterparty_name = counterparty_name;
	}

	public HashMap<String, Integer> getInstrumentDeal() { return instrument_deal; }
	public void setInstrumentDeal(HashMap<String,Integer> instrument_deal) {
		this.instrument_deal = instrument_deal;
	}

}
