package com.db.graduatetraining.analyzer.midtier.domain;

import java.util.Objects;

public class InstrumentWithCount extends Instrument {
	private int count;

	public InstrumentWithCount() {
		super();
	}

	public InstrumentWithCount(int id, String name, int count) {
		super(id, name);
		this.count = count;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!(obj instanceof InstrumentWithCount))
			return false;
		if (obj == this)
			return true;

		final InstrumentWithCount instrument = (InstrumentWithCount) obj;

		return this.count == instrument.count && super.equals(instrument);
	}

	public int hashCode() { // https://www.programcreek.com/2011/07/java-equals-and-hashcode-contract/
		return Objects.hash(getId(), getName(), count); // impl from https://stackoverflow.com/a/18066516
	}


	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
