package com.db.graduatetraining.analyzer.midtier;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Supplies correct DataSource depending on the environment
 */
public class ConnectionManager {
	public static synchronized ConnectionManager getInstance() {
		if (instance == null) {
			instance = new ConnectionManager();
		}
		return instance;
	}
	private static ConnectionManager instance;
	private ConnectionManager() {
		try {
			Context initContext = new InitialContext();
			ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/mainDatabase");
		} catch (NamingException e) {
			Logger.getLogger("ConnectionManager").log(Level.WARNING, "Cannot get connection from container. Call useMysqlDataSource() on ConnectionManager to connect to your local MySQL server.");
		}

	}

	private DataSource ds;
	public Connection getConnection() throws SQLException {
		if (ds == null) {
			return null;
		}
		return ds.getConnection();
	}
	public void setDataSource(DataSource ds) {
		this.ds = ds;
	}

	public void useMysqlDataSource() {
		MysqlDataSource mysqlDs = new MysqlDataSource();
		mysqlDs.setUrl("jdbc:mysql://localhost:3307/db_grad_cs_1917?user=root&password=ppp&useSSL=false");
		ds = mysqlDs;
	}
}
