package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao extends BaseDao<User> {
	public UserDao() {
		super("users", new UserSerializer());
	}

	static protected class UserSerializer implements DomainSerializer<User> {
		@Override
		public User deserialize(ResultSet rs) throws SQLException {
			return new User(rs.getString("user_id"), rs.getString("user_pwd"));
		}
	}
}