package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.Counterparty;
import com.db.graduatetraining.analyzer.midtier.domain.CounterpartyWithCount;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class CounterpartyDao extends BaseDao<Counterparty> {
	public CounterpartyDao() {
		super("counterparty", new CounterpartySerializer());
	}

	static protected class CounterpartySerializer implements DomainSerializer<Counterparty> {
		@Override
		public Counterparty deserialize(ResultSet rs) throws SQLException {
			return new Counterparty(rs.getInt("counterparty_id"), rs.getString("counterparty_name"), rs.getString("counterparty_status"), rs.getTimestamp("counterparty_date_registered"));
		}
	}

	public Collection<CounterpartyWithCount> getAllWithCount() throws SQLException {
		ArrayList<CounterpartyWithCount> list = new ArrayList<CounterpartyWithCount>();
		try (Connection conn = ConnectionManager.getInstance().getConnection();) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT counterparty_id, counterparty_name, counterparty_status, counterparty_date_registered, 0 as `count` FROM counterparty WHERE counterparty_id NOT IN (SELECT deal_counterparty_id FROM deal)\n" +
					"UNION ALL\n" +
					"(SELECT counterparty_id, counterparty_name, counterparty_status, counterparty_date_registered, count(deal_id) AS `count` FROM counterparty\n" +
					"JOIN deal\n" +
					"ON counterparty.counterparty_id = deal.deal_counterparty_id\n" +
					"GROUP BY counterparty_id)");

			while (rs.next()) {
				CounterpartyWithCount current = new CounterpartyWithCount(
						rs.getInt("counterparty_id"),
						rs.getString("counterparty_name"),
						rs.getString("counterparty_status"),
						rs.getTimestamp("counterparty_date_registered"),
						rs.getInt("count")
				);
				list.add(current);
			}
		}
		return list;
	}
}
