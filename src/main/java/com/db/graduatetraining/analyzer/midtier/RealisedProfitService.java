package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.domain.realisedProfit;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;


public abstract class RealisedProfitService<T extends DomainObject> {
	public static Collection<realisedProfit> getAll() throws SQLException {
		ArrayList<realisedProfit> list = new ArrayList<realisedProfit>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("SELECT c.counterparty_name, i.instrument_name, d.deal_amount, d.deal_quantity, d.deal_type\n" +
					"FROM deal d, counterparty c, instrument i\n" +
					"WHERE d.deal_counterparty_id = c.counterparty_id AND i.instrument_id = d.deal_instrument_id  ;\n" +
					"\n"));

			Double profit = 0.0;
			while (rs.next()) {
				String name = rs.getString("counterparty_name");
				rs.previous();
				profit = 0.0;

				while (rs.next()) {
					if (name.equals(rs.getString("counterparty_name"))) {
						if (rs.getString("deal_type").equals("B")) {
							profit = profit + (rs.getDouble("deal_amount") * rs.getInt("deal_quantity"));
						} else {
							profit = profit - (rs.getDouble("deal_amount") * rs.getInt("deal_quantity"));
						}
					} else {
						rs.previous();
						realisedProfit current = new realisedProfit(
								rs.getString("counterparty_name"),
								profit);
						list.add(current);
						break;
					}
				}
			}

			rs.previous();
			realisedProfit current = new realisedProfit(
					rs.getString("counterparty_name"),
					profit);
			list.add(current);
		}
		return list;
	}
}

