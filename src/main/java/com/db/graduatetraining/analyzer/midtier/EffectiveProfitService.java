package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.domain.EffectiveProfit;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public abstract class EffectiveProfitService <T extends DomainObject>{
	public static Collection<EffectiveProfit> getAll() throws SQLException {
		ArrayList<EffectiveProfit> list = new ArrayList<EffectiveProfit>();
		try (Connection conn = ConnectionManager.getInstance().getConnection()) {
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			Statement stmt3 = conn.createStatement();
			Statement stmt4 = conn.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("SELECT c.counterparty_name, i.instrument_name, d.deal_amount, d.deal_quantity, d.deal_type\n" +
					"FROM deal d, counterparty c, instrument i\n" +
					"WHERE d.deal_counterparty_id = c.counterparty_id AND i.instrument_id = d.deal_instrument_id  ;\n"));

			ResultSet rs2 = stmt2.executeQuery(String.format(" Select A.counterparty_name, A.instrument_name, A.Total_Shares_Bought, B.Total_Shares_Sold, (A.Total_Shares_Bought -B.Total_Shares_Sold) as Open_Position\n" +
					"FROM(Select   c.counterparty_name, i.instrument_name,  sum(d.deal_quantity) as Total_Shares_Bought \n" +
					"from deal d, counterparty c, instrument i\n" +
					"where d.deal_counterparty_id = c.counterparty_id AND i.instrument_id = d.deal_instrument_id AND d.deal_type = 'B'\n" +
					"group by d.deal_counterparty_id, d.deal_instrument_id) A\n" +
					"LEFT OUTER JOIN\n" +
					"(Select   c.counterparty_name, i.instrument_name,  sum(d.deal_quantity) as Total_Shares_Sold \n" +
					"from deal d, counterparty c, instrument i\n" +
					"where d.deal_counterparty_id = c.counterparty_id AND i.instrument_id = d.deal_instrument_id AND d.deal_type = 'S'\n" +
					"group by d.deal_counterparty_id, d.deal_instrument_id) B on A.counterparty_name = B.counterparty_name AND A.instrument_name = B.instrument_name; "));

			ResultSet rs3 = stmt3.executeQuery("SELECT A.instrument_name, A.Closing_Buy_Price, B.Closing_Sell_Price FROM (SELECT  i.instrument_name, max(d1.deal_amount) as Closing_Buy_Price\n" +
					"FROM instrument i, deal d1\n" +
					"WHERE d1.deal_time = (SELECT MAX(d2.deal_time) FROM deal d2 WHERE d1.deal_id = d2.deal_id) AND i.instrument_id = d1.deal_instrument_id AND d1.deal_type = \"B\"\n" +
					"GROUP BY  d1.deal_instrument_id ) A \n" +
					"LEFT OUTER JOIN\n" +
					"(SELECT  i.instrument_name, max(d3.deal_amount) as Closing_Sell_Price\n" +
					"FROM instrument i, deal d3\n" +
					"WHERE d3.deal_time = (SELECT MAX(d4.deal_time) FROM deal d4 WHERE d3.deal_id = d4.deal_id) AND i.instrument_id = d3.deal_instrument_id AND d3.deal_type = \"S\"\n" +
					"GROUP BY  d3.deal_instrument_id ) B on A.instrument_name = B.instrument_name; ");


			double totalProfit = 0.0;
			Double addedAmt = 0.0;
			Double totalAddedPosition = 0.0;

			HashMap<String, Double> FinalCosts = new HashMap<String, Double>();

			while (rs.next()) {
				String name = rs.getString("counterparty_name");
				rs.previous();
				while (rs.next()) {
					if (name.equals(rs.getString("counterparty_name"))) {
						if (rs.getString("deal_type").equals("B")) {
							totalProfit = totalProfit + (rs.getDouble("deal_amount") * rs.getInt("deal_quantity"));
						} else {
							totalProfit = totalProfit - (rs.getDouble("deal_amount") * rs.getInt("deal_quantity"));
						}
					} else {
						rs.previous();
						break;
					}
				}
				FinalCosts.put(name, totalProfit);
				totalProfit = 0.0;
			}

			while (rs2.next()) {
				String name = rs2.getString("counterparty_name");
				rs2.previous();
				while (rs2.next()) {
					String instrumentN = rs2.getString("instrument_name");
					if (name.equals(rs2.getString("counterparty_name"))) {
						String instrumentName = rs2.getString("instrument_name");
						rs3.first();
						rs3.previous();
						while (rs3.next()) {
							if (instrumentName.equals(rs3.getString("instrument_name"))) {
								if (rs2.getInt("Open_Position") < 0) {
									addedAmt = rs2.getInt("Open_Position") * rs3.getDouble("Closing_Sell_Price");
									break;
								} else {
									addedAmt = rs2.getInt("Open_Position") * rs3.getDouble("Closing_Buy_Price");
									break;
								}
							}
						}
					} else {
						rs2.previous();
						break;
					}
					totalAddedPosition = totalAddedPosition + addedAmt;
					addedAmt = 0.0;
				}
				if (FinalCosts.containsKey(name)) {
					FinalCosts.put(name, FinalCosts.get(name) + totalAddedPosition);
				}
				totalAddedPosition = 0.0;
			}
			for (String key : FinalCosts.keySet()) {
				Double value = FinalCosts.get(key);
				EffectiveProfit current = new EffectiveProfit(key, value);
				list.add(current);
			}
		}
		return list;
	}
}
