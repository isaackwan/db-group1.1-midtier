package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.domain.Counterparty;
import com.db.graduatetraining.analyzer.midtier.domain.Deal;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;

public class CounterpartySerializerTest {
	private class CounterpartyDaoExposed extends CounterpartyDao {
		public class CounterpartySerializerExposed extends CounterpartySerializer {}
		public CounterpartySerializerExposed serializer = new CounterpartySerializerExposed();
	}

	public CounterpartyDaoExposed.CounterpartySerializerExposed serializer = new CounterpartyDaoExposed().serializer;

	@Test
	public void deserialize() throws SQLException {
		final Mockery context = new Mockery();
		final ResultSet rs = context.mock(ResultSet.class);

		context.checking(new Expectations() {{
			allowing(rs).getInt("counterparty_id");
			will(returnValue(124));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getString("counterparty_name");
			will(returnValue("No name"));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getString("counterparty_status");
			will(returnValue("Z"));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getTimestamp("counterparty_date_registered");
			will(returnValue(Timestamp.valueOf("2018-08-13 12:34:41")));
		}});

		final Counterparty counterparty = new Counterparty(124, "No name", "Z", Timestamp.valueOf("2018-08-13 12:34:41"));
		final Counterparty deserialized = serializer.deserialize(rs);
		assertEquals(counterparty, deserialized);

		context.assertIsSatisfied();
	}
}