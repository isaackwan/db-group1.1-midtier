package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.domain.User;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class UserSerializerTest {
	private class UserDaoExposed extends UserDao {
		public class UserSerializerExposed extends UserSerializer {}
		public UserSerializerExposed serializer = new UserSerializerExposed();
	}

	public UserDaoExposed.UserSerializerExposed serializer = new UserDaoExposed().serializer;

	@Test
	public void deserialize() throws SQLException {
		final Mockery context = new Mockery();
		final ResultSet rs = context.mock(ResultSet.class);

		context.checking(new Expectations() {{
			allowing(rs).getString("user_id");
			will(returnValue("1222"));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getString("user_pwd");
			will(returnValue("Not a name"));
		}});

		final User user = new User("1222", "Not a name");
		final User deserialized = serializer.deserialize(rs);
		assertEquals(user, deserialized);

		context.assertIsSatisfied();
	}
}