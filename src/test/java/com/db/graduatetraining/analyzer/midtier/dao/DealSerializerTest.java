package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.domain.Deal;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import static org.junit.Assert.*;

public class DealSerializerTest {
	private class DealDaoExposed extends DealDao {
		public class DealSerializerExposed extends DealSerializer {}
		public DealSerializerExposed dealSerializer = new DealSerializerExposed();
	}

	public DealDaoExposed.DealSerializerExposed dealSerializer = new DealDaoExposed().dealSerializer;

	@Test
	public void deserialize() throws SQLException {
		final Mockery context = new Mockery();
		final ResultSet rs = context.mock(ResultSet.class);

		context.checking(new Expectations() {{
			allowing(rs).getInt("deal_id");
			will(returnValue(12345));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getInt("deal_counterparty_id");
			will(returnValue(42));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getInt("deal_instrument_id");
			will(returnValue(2018));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getString("deal_type");
			will(returnValue("this is cool"));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getBigDecimal("deal_amount");
			will(returnValue(new BigDecimal(56)));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getInt("deal_quantity");
			will(returnValue(27));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getString("deal_time");
			will(returnValue("987"));
		}});

		final Deal deal = new Deal(12345, 42, 2018, "this is cool", new BigDecimal(56), 27, "987");
		final Deal deserialized = dealSerializer.deserialize(rs);
		assertEquals(deal, deserialized);

		context.assertIsSatisfied();
	}
}