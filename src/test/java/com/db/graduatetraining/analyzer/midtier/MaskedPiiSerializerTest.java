package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.MaskedPiiSerializer;
import com.fasterxml.jackson.core.JsonGenerator;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

public class MaskedPiiSerializerTest {
	@Ignore
	@Test
	public void serialize() throws IOException {
		Mockery context = new Mockery();
		final JsonGenerator gen = context.mock(JsonGenerator.class); // cannot mock a concrete class

		final MaskedPiiSerializer maskedPiiSerializer = new MaskedPiiSerializer();

		context.checking(new Expectations() {{
			exactly(1).of(gen).writeString("xxxxx");
		}});

		maskedPiiSerializer.serialize(null, gen, null);

		context.assertIsSatisfied();
	}
}
