package com.db.graduatetraining.analyzer.midtier.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class CounterpartyInstrumentCountTest {
	final CounterpartyInstrumentCount cic1 = new CounterpartyInstrumentCount(4, 9, 10);

	@Test
	public void ciEquals() {
		final CounterpartyInstrumentCount cic2 = new CounterpartyInstrumentCount(4, 9, 10);
		assertEquals(cic1, cic2);
	}

	@Test
	public void ciHashCode() {
		cic1.hashCode();
	}

	@Test
	public void ciGetCounterpartyId() {
		assertEquals(4, cic1.getCounterpartyId());
	}

	@Test
	public void ciSetCounterpartyId() {
		final CounterpartyInstrumentCount cic2 = new CounterpartyInstrumentCount(4, 9, 10);
		final CounterpartyInstrumentCount cic3 = new CounterpartyInstrumentCount(13, 9, 10);
		cic2.setCounterpartyId(13);
		assertEquals(cic3, cic2);
	}

	@Test
	public void ciGetInstrumentId() {
		assertEquals(9, cic1.getInstrumentId());
	}

	@Test
	public void ciSetInstrumentId() {
		final CounterpartyInstrumentCount cic2 = new CounterpartyInstrumentCount(4, 9, 10);
		final CounterpartyInstrumentCount cic3 = new CounterpartyInstrumentCount(4, 12, 10);
		cic2.setInstrumentId(12);
		assertEquals(cic3, cic2);
	}

	@Test
	public void ciGetCount() {
		assertEquals(10, cic1.getCount());
	}

	@Test
	public void ciSetCount() {
		final CounterpartyInstrumentCount cic2 = new CounterpartyInstrumentCount(4, 9, 10);
		final CounterpartyInstrumentCount cic3 = new CounterpartyInstrumentCount(4, 9, 11);
		cic2.setCount(11);
		assertEquals(cic3, cic2);
	}
}