package com.db.graduatetraining.analyzer.midtier.domain;

import org.junit.Test;

import java.sql.Timestamp;
import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

	public class CounterpartyTest {
	final Counterparty c1 = new Counterparty(123, "test", "S", Timestamp.valueOf("2018-08-13 11:22:33"));

	@Test
	public void counterpartyEqual() {
		final Counterparty c2 = new Counterparty(123, "test", "S", Timestamp.valueOf("2018-08-13 11:22:33"));
		assertThat(c1, equalTo(c2));
	}

	@Test
	public void counterpartyHashCode() {
		c1.hashCode();
	}

	@Test
	public void counterpartyGetId() {
		assertThat(c1.getId(), equalTo(123));
	}

	@Test
	public void counterpartySetId() {
		final Counterparty c2 = new Counterparty(123, "test", "S", Timestamp.valueOf("2018-08-13 11:22:33"));
		c2.setId(124);
		assertThat(c2.getId(), equalTo(124));
	}

	@Test
	public void counterpartyGetName() {
		assertThat(c1.getName(), equalTo("test"));
	}

	@Test
	public void counterpartySetName() {
		final Counterparty c2 = new Counterparty(123, "test", "S", Timestamp.valueOf("2018-08-13 11:22:33"));
		c2.setName("ttt");
		assertThat(c2.getName(), equalTo("ttt"));
	}

	@Test
	public void counterpartyGetStatus() {
		assertThat(c1.getStatus(), equalTo("S"));
	}

	@Test
	public void counterpartySetStatus() {
		final Counterparty c2 = new Counterparty(123, "test", "S", Timestamp.valueOf("2018-08-13 11:22:33"));
		c2.setStatus("T");
		assertThat(c2.getStatus(), equalTo("T"));
	}

	@Test
	public void counterpartyGetDateRegistered() {
		assertEquals(c1.getDateRegistered(), Timestamp.valueOf("2018-08-13 11:22:33"));
	}

	@Test
	public void counterpartySetDateRegistered() {
		final Timestamp ts = Timestamp.valueOf("2018-08-10 11:21:33");
		final Counterparty c2 = new Counterparty(123, "test", "S", Timestamp.valueOf("2018-08-13 11:22:33"));
		c2.setDateRegistered(ts);
		assertThat(c2.getDateRegistered(), equalTo(ts));
	}
}