package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.domain.Instrument;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class InstrumentSerializerTest {
	private class InstrumentDaoExposed extends InstrumentDao {
		public class InstrumentSerializerExposed extends InstrumentSerializer {}
		public InstrumentSerializerExposed serializer = new InstrumentSerializerExposed();
	}

	public InstrumentDaoExposed.InstrumentSerializerExposed serializer = new InstrumentDaoExposed().serializer;

	@Test
	public void deserialize() throws SQLException {
		final Mockery context = new Mockery();
		final ResultSet rs = context.mock(ResultSet.class);

		context.checking(new Expectations() {{
			allowing(rs).getInt("instrument_id");
			will(returnValue(1254));
		}});

		context.checking(new Expectations() {{
			allowing(rs).getString("instrument_name");
			will(returnValue("No name"));
		}});

		final Instrument instrument = new Instrument(1254, "No name");
		final Instrument deserialized = serializer.deserialize(rs);
		assertEquals(instrument, deserialized);

		context.assertIsSatisfied();
	}
}