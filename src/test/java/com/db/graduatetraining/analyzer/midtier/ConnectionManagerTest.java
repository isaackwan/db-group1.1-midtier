package com.db.graduatetraining.analyzer.midtier;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ConnectionManagerTest {

	@Test
	public void firstCallToSingletonCreatesANewInstance() {
		assertNotNull("Returns an instance of ConnectionManager", ConnectionManager.getInstance());
	}

	@Test
	public void repeatedCallsToSingletonMethodsGetTheSameInstance() {
		assertEquals(ConnectionManager.getInstance(), ConnectionManager.getInstance());
	}

	/**
	 * try to get a DS from ConnectionManager
	 * Will use MySQL driver when DS is not obtained from container
	 * make sure you have a MySQL server running or else test will fail
	 */
	@Test
	public void getDataSource() throws SQLException {
		assertThat(ConnectionManager.getInstance().getConnection(), is(nullValue()));
	}

	@Test
	public void setDataSource() throws SQLException {
		Mockery context = new Mockery();
		final DataSource ds = context.mock(DataSource.class);
		final Connection conn = context.mock(Connection.class);
		final ConnectionManager instance = ConnectionManager.getInstance();
		instance.setDataSource(ds);
		context.checking(new Expectations() {{
			atLeast(1).of(ds).getConnection();
			will(returnValue(conn));
		}});
		context.checking(new Expectations() {{
			assertEquals("Returned connection is the one provided by mock", conn, instance.getConnection());
		}});
		context.assertIsSatisfied();
	}
}