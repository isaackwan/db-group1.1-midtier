package com.db.graduatetraining.analyzer.midtier;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.SqlCheckService;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;

public class SqlCheckServiceTest {

	@Test
	public void getDatabaseVersion() throws SQLException {
		Mockery context = new Mockery();
		final DataSource ds = context.mock(DataSource.class);
		final Connection conn = context.mock(Connection.class);
		final Statement stmt = context.mock(Statement.class);
		final ResultSet rs = context.mock(ResultSet.class);
		final ConnectionManager service = ConnectionManager.getInstance();
		service.setDataSource(ds);

		context.checking(new Expectations() {{
			atLeast(1).of(ds).getConnection();
			will(returnValue(conn));
		}});

		context.checking(new Expectations() {{
			allowing(conn).createStatement();
			will(returnValue(stmt));
		}});

		context.checking(new Expectations() {{
			atLeast(1).of(stmt).executeQuery("SELECT VERSION();");
			will(returnValue(rs));
		}});

		context.checking(new Expectations() {{
			exactly(1).of(rs).next();
			atLeast(1).of(rs).getString(1);
			will(returnValue("nice job"));
		}});

		context.checking(new Expectations() {{
			allowing(conn).close();
		}});

		assertEquals("nice job", new SqlCheckService().getDatabaseVersion());
		context.assertIsSatisfied();
	}
}