package com.db.graduatetraining.analyzer.midtier.dao;

import com.db.graduatetraining.analyzer.midtier.ConnectionManager;
import com.db.graduatetraining.analyzer.midtier.DomainSerializer;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class BaseDaoTest {

	@Test
	public void getAll() throws SQLException {
		Mockery context = new Mockery();
		final DataSource ds = context.mock(DataSource.class);
		final Connection conn = context.mock(Connection.class);
		final Statement stmt = context.mock(Statement.class);
		final ResultSet rs = context.mock(ResultSet.class);
		final DomainSerializer<Fake> domainSerializer = context.mock(DomainSerializer.class);
		final ConnectionManager service = ConnectionManager.getInstance();
		final Fake fake = new Fake();
		service.setDataSource(ds);

		context.checking(new Expectations() {{
			atLeast(1).of(ds).getConnection();
			will(returnValue(conn));
		}});

		context.checking(new Expectations() {{
			allowing(conn).createStatement();
			will(returnValue(stmt));
		}});

		context.checking(new Expectations() {{
			atLeast(1).of(stmt).executeQuery("SELECT * FROM fake");
			will(returnValue(rs));
		}});

		context.checking(new Expectations() {{
			exactly(1).of(rs).next();
			will(returnValue(true));
		}});

		context.checking(new Expectations() {{
			atLeast(1).of(domainSerializer).deserialize(rs);
			will(returnValue(fake));
		}});

		context.checking(new Expectations() {{
			exactly(1).of(rs).next();
			will(returnValue(false));
		}});

		context.checking(new Expectations() {{
			allowing(conn).close();
		}});

		final FakeDao dao = new FakeDao(domainSerializer);
		final Collection<Fake> all = dao.getAll();
		assertThat(all, is(Arrays.asList(fake)));
		context.assertIsSatisfied();
	}
}

class Fake implements DomainObject {}

class FakeDao extends BaseDao<Fake> {
	public FakeDao(DomainSerializer ds) {
		super("fake", ds);
	}
}